package com.company.l1testapp.di.component;

import android.content.Context;
import android.content.res.Resources;

import com.company.l1testapp.di.module.ApplicationModule;
import com.company.l1testapp.di.module.PresentationModule;
import com.company.l1testapp.di.qualifier.ApplicationContext;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    PresentationComponent plusPresentationComponent();
}