package com.company.l1testapp.di.component;

import com.company.l1testapp.di.module.PresentationModule;
import com.company.l1testapp.ui.main.MainActivity;

import dagger.Subcomponent;

@Subcomponent(modules = {PresentationModule.class})
public interface PresentationComponent {


    void inject(MainActivity mainActivity);
}