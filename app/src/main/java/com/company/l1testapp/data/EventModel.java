package com.company.l1testapp.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EventModel {

    @SerializedName("events")
    public ArrayList<Event> list;

    static public class Event {
        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("active")
        public Boolean active;
    }
}
