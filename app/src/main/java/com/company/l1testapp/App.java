package com.company.l1testapp;

import android.app.Application;
import android.content.Context;

import com.company.l1testapp.di.component.ApplicationComponent;
import com.company.l1testapp.di.component.DaggerApplicationComponent;
import com.company.l1testapp.di.component.PresentationComponent;
import com.company.l1testapp.di.module.ApplicationModule;

import timber.log.Timber;

public class App extends Application {

    private ApplicationComponent applicationComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public PresentationComponent plusPresentationComponent() {
        return getComponent().plusPresentationComponent();
    }

}
