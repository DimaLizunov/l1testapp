package com.company.l1testapp.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.company.l1testapp.R;
import com.company.l1testapp.ui.base.BaseActivity;

import javax.inject.Inject;

import timber.log.Timber;

import static com.company.l1testapp.ui.main.MainContract.*;

public class MainActivity extends BaseActivity<MainContract.Presenter, MainContract.View>
        implements
        MainContract.View {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityComponent().inject(this);



        Timber.d("Timber getData %s", mPresenter.getData(this));
        Timber.d("Timber getData list %s", mPresenter.getData(this).list.get(2).name);
    }

    @Inject
    protected void injectPresenter(MainPresenter presenter) {
        super.injectPresenter(presenter);
    }
}
