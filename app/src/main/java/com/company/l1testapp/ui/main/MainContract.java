package com.company.l1testapp.ui.main;

import android.content.Context;

import com.company.l1testapp.data.EventModel;
import com.company.l1testapp.ui.base.BaseContract;

public interface MainContract {

    interface View extends BaseContract.View {

    }

    interface Presenter extends BaseContract.Presenter<View> {

        EventModel getData(Context context);

    }

}
