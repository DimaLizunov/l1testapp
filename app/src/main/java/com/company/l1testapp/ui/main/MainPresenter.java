package com.company.l1testapp.ui.main;

import android.content.Context;

import com.company.l1testapp.R;
import com.company.l1testapp.data.EventModel;
import com.company.l1testapp.ui.base.BasePresenter;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

public class MainPresenter extends BasePresenter<MainContract.View>
        implements MainContract.Presenter {

    @Inject
    MainPresenter() {
    }

    public String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

    public String getStringFromJson(Context context) {
        return inputStreamToString(context.getResources().openRawResource(R.raw.events));
    }

    @Override
    public EventModel getData(Context context) {
        return new Gson().fromJson(getStringFromJson(context), EventModel.class);
    }


}
