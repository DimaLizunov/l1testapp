package com.company.l1testapp.ui.base;

import android.support.annotation.Nullable;

public class BasePresenter<T extends BaseContract.View> implements BaseContract.Presenter<T> {

    private @Nullable
    T mView;

    @Override
    public void attachView(T view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }

    @Nullable
    public T getView() {
        return mView;
    }
}
