package com.company.l1testapp.ui.base;

public interface BaseContract {

    interface View {

    }

    interface Presenter<T extends View> {

        void attachView(T view);

        void detachView();
    }
}
