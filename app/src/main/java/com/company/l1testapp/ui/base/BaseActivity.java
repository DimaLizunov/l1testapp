package com.company.l1testapp.ui.base;

import android.support.v7.app.AppCompatActivity;

import com.company.l1testapp.App;
import com.company.l1testapp.di.component.PresentationComponent;

public abstract class BaseActivity<P extends BaseContract.Presenter<V>, V extends BaseContract.View> extends AppCompatActivity
        implements BaseContract.View {

    protected P mPresenter;

    public PresentationComponent activityComponent() {
        return App.get(this).plusPresentationComponent();
    }

    @SuppressWarnings("unchecked")
    protected void injectPresenter(P presenter) {
        mPresenter = presenter;
        mPresenter.attachView((V) this);
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }

}
